## Add styles to Admin 

```php
function admin_styles() {
	wp_enqueue_style('admin-styles', get_template_directory_uri().'/assets/admin/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_styles');
```


## Add scripts to Admin on selected pages

```php
function admin_scripts($hook) {
	if( $hook != 'edit.php' && $hook != 'post.php' && $hook != 'post-new.php' ) 
		return;
	wp_enqueue_script( 'custom-js', get_template_directory_uri().'/assets/admin/admin.js');
}
add_action('admin_enqueue_scripts', 'admin_scripts');
```

## eg: ACF Button Group Colours - Use new label classes to style button colour

In ACF, button choices:

green : Green  
red : Red  
blue : Blue  

```javascript
jQuery(document).ready(function($) { 
	if($('.acf-button-group').length){
		$('.acf-button-group > label').each(function () {
			var color = $(this).find('input').val();
			$(this).addClass(color);
		});
	}
});
```

```css
.acf-button-group label.selected.red, .acf-button-group label.red:hover, .acf-button-group label.selected.red:hover {
	border-color: red;
	background-color: red;
	color: white;
}
.acf-button-group label.selected.green, .acf-button-group label.green:hover, .acf-button-group label.selected.green:hover {
	border-color: green;
	background-color: green;
	color: white;
}
.acf-button-group label.selected.blue, .acf-button-group label.blue:hover, .acf-button-group label.selected.blue:hover {
	border-color: blue;
	background-color: blue;
	color: white;
}
```